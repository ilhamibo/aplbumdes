/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View.Pelanggan;

import Controller.pelanggan_c;
import Model.Pelanggan;
import javax.swing.JOptionPane;

/**
 *
 * @author ilham syaiful akbar
 */
public class FormEditPelanggan_v extends javax.swing.JDialog {
    
    pelanggan_c function = new pelanggan_c();
    Pelanggan pelEdit = null;
    String globalId_Pelanggan = null;
    /**
     * Creates new form FormAddPelanggan
     */
    public FormEditPelanggan_v(java.awt.Frame parent, boolean modal) {
       super(parent, modal);
       initComponents();
       getEditDataPelanggan("127");
        setLocationRelativeTo(this);
    }

    FormEditPelanggan_v() {
        initComponents();
        setLocationRelativeTo(this);
    }

    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        namaPelanggan = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        carabayarcombo = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        titikPelanggan = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        desaPelanggan = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        rtPelanggan = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        rwPelanggan = new javax.swing.JTextField();
        btnSimpan = new javax.swing.JButton();
        btnBatal = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        statuscombo = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel1.setText("Nama  :");

        namaPelanggan.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        namaPelanggan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                namaPelangganActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel2.setText("Cara Bayar  :");

        carabayarcombo.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        carabayarcombo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "tunai", "transfer", "cicil", "Item 4" }));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel3.setText("Titik/Wilayah  :");

        titikPelanggan.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        titikPelanggan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                titikPelangganActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel4.setText("Desa :");

        desaPelanggan.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        desaPelanggan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                desaPelangganActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel5.setText("RT :");

        rtPelanggan.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        rtPelanggan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rtPelangganActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel6.setText("RW :");

        rwPelanggan.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        rwPelanggan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rwPelangganActionPerformed(evt);
            }
        });

        btnSimpan.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        btnSimpan.setText("Simpan");
        btnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanActionPerformed(evt);
            }
        });

        btnBatal.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        btnBatal.setText("Batal");

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabel7.setText("Status");

        statuscombo.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        statuscombo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Berlangganan", "Tidak Berlangganan" }));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(namaPelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(carabayarcombo, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnSimpan)
                        .addGap(18, 18, 18)
                        .addComponent(btnBatal))
                    .addComponent(desaPelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(titikPelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(statuscombo, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(rtPelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(74, 74, 74)
                                .addComponent(jLabel6)))
                        .addGap(60, 60, 60)
                        .addComponent(rwPelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(66, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(namaPelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(carabayarcombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(titikPelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(desaPelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(rtPelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(rwPelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(statuscombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSimpan)
                    .addComponent(btnBatal))
                .addGap(19, 19, 19))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void getEditDataPelanggan(String id_pelanggan){
        try {
            globalId_Pelanggan = id_pelanggan;
            pelEdit = function.editGetPelanggan(id_pelanggan);
            namaPelanggan.setText(pelEdit.getPelanggan_nama());
            carabayarcombo.setSelectedIndex(Integer.parseInt(pelEdit.getPelanggan_carabayar()));
            desaPelanggan.setText(pelEdit.getPelanggan_desa());
            titikPelanggan.setText(String.valueOf(pelEdit.getPelanggan_titik()));
            rtPelanggan.setText(pelEdit.getPelanggan_rt());
            rwPelanggan.setText(pelEdit.getPelanggan_rw());
            statuscombo.setSelectedIndex(pelEdit.getPelanggan_statuslangganan());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, id_pelanggan+", "+pelEdit.getPelanggan_nama());
        }
    }
    
    private void titikPelangganActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_titikPelangganActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_titikPelangganActionPerformed

    private void desaPelangganActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_desaPelangganActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_desaPelangganActionPerformed

    private void rtPelangganActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rtPelangganActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rtPelangganActionPerformed

    private void rwPelangganActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rwPelangganActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rwPelangganActionPerformed

    private void btnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanActionPerformed
//        String pelanggan_nama, pelanggan_desa, pelanggan_rt, pelanggan_rw, pelanggan_carabayar, pelanggan_titik;
//        pelanggan_nama = namaPelanggan.getText();
//        pelanggan_desa = desaPelanggan.getText();
//        pelanggan_rt = rtPelanggan.getText();
//        pelanggan_rw = rwPelanggan.getText();
//        pelanggan_carabayar = carabayarcombo.getSelectedItem().toString();
//        pelanggan_titik = titikPelanggan.getText();
        
        try {
            Pelanggan pel = new Pelanggan();
            
            pel.setPelanggan_nama(namaPelanggan.getText());
            pel.setPelanggan_carabayar(String.valueOf(carabayarcombo.getSelectedIndex()));
            pel.setPelanggan_desa(desaPelanggan.getText());
            pel.setPelanggan_titik(Byte.parseByte(titikPelanggan.getText()));
            pel.setPelanggan_rt(rtPelanggan.getText());
            pel.setPelanggan_rw(rwPelanggan.getText());
            pel.setPelanggan_statuslangganan(Byte.parseByte("1"));
            function.editPelanggan(globalId_Pelanggan,pel);
            JOptionPane.showMessageDialog(this, "Data Pelanggan Berhasil Diubah");
            this.dispose();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }//GEN-LAST:event_btnSimpanActionPerformed

    private void namaPelangganActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_namaPelangganActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_namaPelangganActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormEditPelanggan_v.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormEditPelanggan_v.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormEditPelanggan_v.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormEditPelanggan_v.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FormEditPelanggan_v dialogAddPelanggan = new FormEditPelanggan_v(new javax.swing.JFrame(), true);
                dialogAddPelanggan.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialogAddPelanggan.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBatal;
    private javax.swing.JButton btnSimpan;
    private javax.swing.JComboBox<String> carabayarcombo;
    private javax.swing.JTextField desaPelanggan;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField namaPelanggan;
    private javax.swing.JTextField rtPelanggan;
    private javax.swing.JTextField rwPelanggan;
    private javax.swing.JComboBox<String> statuscombo;
    private javax.swing.JTextField titikPelanggan;
    // End of variables declaration//GEN-END:variables
}
