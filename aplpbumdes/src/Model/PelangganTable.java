/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.List;
import javax.swing.event.EventListenerList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ilham syaiful akbar
 */
public class PelangganTable extends AbstractTableModel {

    private List<Pelanggan> pelList;
    private String[] fieldTable = {"Kode", "Nama","Cara Pembayaran", "Wilayah"};// field pada tabel tenant yang ditampilkan

    public PelangganTable(List<Pelanggan> pelList) {
        this.pelList = pelList;
    }

    public List<Pelanggan> getPelList() {
        return pelList;
    }

    public void setPelList(List<Pelanggan> pelList) {
        this.pelList = pelList;
    }

    public String[] getFieldTable() {
        return fieldTable;
    }

    public void setFieldTable(String[] fieldTable) {
        this.fieldTable = fieldTable;
    }

    public EventListenerList getListenerList() {
        return listenerList;
    }

    public void setListenerList(EventListenerList listenerList) {
        this.listenerList = listenerList;
    }
           
    @Override
    public int getRowCount() {
        return pelList.size();
    }

    @Override
    public int getColumnCount() {
        return fieldTable.length;
    }
    

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return pelList.get(rowIndex).getPelanggan_no();
        } else if (columnIndex == 1) {
            return pelList.get(rowIndex).getPelanggan_nama();
        } else if (columnIndex == 2) {
            return pelList.get(rowIndex).getPelanggan_carabayar();
        } else if (columnIndex == 3) {
            return pelList.get(rowIndex).getPelanggan_titik();
        } else if (columnIndex == 4) {
            return pelList.get(rowIndex).getPelanggan_desa();
        } else if (columnIndex == 5) {
            return pelList.get(rowIndex).getPelanggan_rt();
        } else if (columnIndex == 6) {
            return pelList.get(rowIndex).getPelanggan_rw();
        } else if (columnIndex == 7) {
            return pelList.get(rowIndex).getPelanggan_statuslangganan();
        }

        return pelList;
    }

    @Override
    public String getColumnName(int column) {
        return fieldTable[column];
    }
    
    
}
