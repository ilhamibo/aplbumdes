/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author ilham syaiful akbar
 */
public class Pelanggan {
    
    private String pelanggan_no;
    private String pelanggan_nama;
    private String pelanggan_carabayar;
    private byte pelanggan_titik;
    private String pelanggan_desa;
    private String pelanggan_rt;
    private String pelanggan_rw;
    private byte pelanggan_statuslangganan;

    public Pelanggan(String pelanggan_no, String pelanggan_nama, String pelanggan_carabayar, byte pelanggan_titik, String pelanggan_desa, String pelanggan_rt, String pelanggan_rw, byte pelanggan_statuslangganan) {
        this.pelanggan_no = pelanggan_no;
        this.pelanggan_nama = pelanggan_nama;
        this.pelanggan_carabayar = pelanggan_carabayar;
        this.pelanggan_titik = pelanggan_titik;
        this.pelanggan_desa = pelanggan_desa;
        this.pelanggan_rt = pelanggan_rt;
        this.pelanggan_rw = pelanggan_rw;
        this.pelanggan_statuslangganan = pelanggan_statuslangganan;
    }

    public Pelanggan() {
        
    }

    public String getPelanggan_no() {
        return pelanggan_no;
    }

    public void setPelanggan_no(String pelanggan_no) {
        this.pelanggan_no = pelanggan_no;
    }

    public String getPelanggan_nama() {
        return pelanggan_nama;
    }

    public void setPelanggan_nama(String pelanggan_nama) {
        this.pelanggan_nama = pelanggan_nama;
    }

    public String getPelanggan_carabayar() {
        return pelanggan_carabayar;
    }

    public void setPelanggan_carabayar(String pelanggan_carabayar) {
        this.pelanggan_carabayar = pelanggan_carabayar;
    }

    public byte getPelanggan_titik() {
        return pelanggan_titik;
    }

    public void setPelanggan_titik(byte pelanggan_titik) {
        this.pelanggan_titik = pelanggan_titik;
    }

    public String getPelanggan_desa() {
        return pelanggan_desa;
    }

    public void setPelanggan_desa(String pelanggan_desa) {
        this.pelanggan_desa = pelanggan_desa;
    }

    public String getPelanggan_rt() {
        return pelanggan_rt;
    }

    public void setPelanggan_rt(String pelanggan_rt) {
        this.pelanggan_rt = pelanggan_rt;
    }

    public String getPelanggan_rw() {
        return pelanggan_rw;
    }

    public void setPelanggan_rw(String pelanggan_rw) {
        this.pelanggan_rw = pelanggan_rw;
    }

    public byte getPelanggan_statuslangganan() {
        return pelanggan_statuslangganan;
    }

    public void setPelanggan_statuslangganan(byte pelanggan_statuslangganan) {
        this.pelanggan_statuslangganan = pelanggan_statuslangganan;
    }
    
    
    
}
