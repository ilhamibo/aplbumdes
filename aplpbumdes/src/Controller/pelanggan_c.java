/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Pelanggan;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author ilham syaiful akbar
 */
public class pelanggan_c {
    Connection con = ConnectionDb.ConnectDB();
    
    //method untuk meng-get field tenant
    public static List<Pelanggan> getPelanggan() {
        List<Pelanggan> pelList = new ArrayList<Pelanggan>();
        return pelList;
    }
    
    //method untuk menampilkan semua daftar tenant 
    public static List<Pelanggan> viewAllPelanggan() throws SQLException, ClassNotFoundException {
        List<Pelanggan> pelList;
        Connection con = ConnectionDb.ConnectDB(); //melakukan koneksi dengan memanggil kelas ConnectDB
        try {
            Statement stat = con.createStatement();
            ResultSet rs = stat.executeQuery("SELECT pelanggan_no, pelanggan_nama, pelanggan_carabayar, "
                    + "pelanggan_titik FROM pelanggan "); // Query untuk menampilkan hanya id_tenat dan nama_tenant yang ada pada tabel tenant di database
            pelList = new ArrayList<>();
            while (rs.next()) {
                Pelanggan pel = new Pelanggan();
                pel.setPelanggan_no(rs.getString("pelanggan_no"));
                pel.setPelanggan_nama(rs.getString("pelanggan_nama"));
                pel.setPelanggan_carabayar(rs.getString("pelanggan_carabayar"));
                pel.setPelanggan_titik(rs.getByte("pelanggan_titik"));
                pelList.add(pel);
            }
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return pelList;
    }
    
    public String buildNewIdPelanggan() throws SQLException {
        Statement stat = con.createStatement();
        ResultSet result = stat.executeQuery("SELECT MAX(pelanggan_no) as pelanggan_no FROM pelanggan ");
        result.first();
        String id = result.getString("pelanggan_no");
        int pelanggan_no = Integer.parseInt(id);
        int new_id = pelanggan_no +1;
        return String.valueOf(new_id);
    }
    
    public void addPelanggan(Pelanggan pel)throws SQLException {
        try {
            PreparedStatement query = con.prepareStatement("INSERT INTO pelanggan values(?,?,?,?,?,?,?,?)");
            query.setString(1, buildNewIdPelanggan());
            query.setString(2, pel.getPelanggan_nama());
            query.setString(3, pel.getPelanggan_carabayar());
            query.setByte(4, pel.getPelanggan_titik());
            query.setString(5, pel.getPelanggan_desa());
            query.setString(6, pel.getPelanggan_rt());
            query.setString(7, pel.getPelanggan_rw());
            query.setByte(8, pel.getPelanggan_statuslangganan());
            query.executeUpdate();
        }catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
//        } finally {
//            if (con != null) {
//                con.close();
//            }
//        } 
    }
    
    public Pelanggan editGetPelanggan(String pelanggan_no) throws SQLException {
        try {
            Pelanggan pel = new Pelanggan();
            Statement stat = con.createStatement();
            ResultSet rs = stat.executeQuery("SELECT * FROM pelanggan WHERE pelanggan_no='" + pelanggan_no + "'");
            // Query untuk menampilkan tenant yang akan diedit 
            if (rs.next()) {
                pel.setPelanggan_nama(rs.getString("pelanggan_nama"));
                pel.setPelanggan_carabayar(rs.getString("pelanggan_carabayar"));
                pel.setPelanggan_titik(Byte.parseByte(rs.getString("pelanggan_titik")));
                pel.setPelanggan_desa(rs.getString("pelanggan_desa"));
                pel.setPelanggan_rt(rs.getString("pelanggan_rt"));
                pel.setPelanggan_rw(rs.getString("pelanggan_rw"));
                pel.setPelanggan_statuslangganan(Byte.parseByte(rs.getString("pelanggan_statuslangganan")));
                return pel;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error di method edit Pelanggan " + e.getMessage().toString());
        }
        return null;
    }
    
    public void editPelanggan(String pelanggan_no, Pelanggan pel) throws SQLException {
        try {
            PreparedStatement pst = con.prepareStatement("update pelanggan set pelanggan_nama=?,"+
                    " pelanggan_carabayar=?, pelanggan_titik=?, pelanggan_desa=?, pelanggan_rt=?,"+
                    " pelanggan_rw=?, pelanggan_statuslangganan=? WHERE pelanggan_no=?");
            // Query untuk mengedit / merubah tenant yang ada pada tabel tenant di database
            pst.setString(1, pel.getPelanggan_nama());
            pst.setString(2, pel.getPelanggan_carabayar());
            pst.setByte(3, pel.getPelanggan_titik());
            pst.setString(4, pel.getPelanggan_desa());
            pst.setString(5, pel.getPelanggan_rt());
            pst.setString(6, pel.getPelanggan_rw());
            pst.setByte(7, pel.getPelanggan_statuslangganan());
            pst.setString(8, pelanggan_no);
            pst.executeUpdate();
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null,e.getMessage().toString());
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }
    //method untuk menghapus pelanggan
    public void deletePelanggan(String pelanggan_no) throws SQLException {
        Connection koneksi = null;
        try {
            koneksi = ConnectionDb.ConnectDB();
            PreparedStatement pst = koneksi.prepareStatement("delete from pelanggan where pelanggan_no=?");
            // Query untuk menghapus tenant yang ada dalam tabel tenant di database
            pst.setString(1, pelanggan_no);
            pst.executeUpdate();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (koneksi != null) {
                koneksi.close();
            }
        }
    }
    
    //method untuk pencarian pelanggan 
    public static List<Pelanggan> pencarianPelanggan(String pelanggan_nama) {
        List<Pelanggan> pelList = new ArrayList<>();
        try {
            Connection koneksi = ConnectionDb.ConnectDB();
            Statement statement = koneksi.createStatement();
            ResultSet result = statement.executeQuery("SELECT pelanggan_no, pelanggan_nama, pelanggan_carabayar, "
                    + "pelanggan_titik FROM pelanggan WHERE "
                    + " pelanggan_nama LIKE '%" + pelanggan_nama + "%'");
            // Query untuk menampilkan tenant yang dicari berdasarkan nama tenant
            while (result.next()) {
                Pelanggan pel = new Pelanggan();
                pel.setPelanggan_no(result.getString("pelanggan_no"));
                pel.setPelanggan_nama(result.getString("pelanggan_nama"));
                pel.setPelanggan_carabayar(result.getString("pelanggan_carabayar"));
                pel.setPelanggan_titik(Byte.parseByte(result.getString("pelanggan_titik")));
                pelList.add(pel);
            }
        } catch (Exception e) {
            
        }
        return pelList;
    }
}
